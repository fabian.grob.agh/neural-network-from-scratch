# CUDA Neural Network

My project for the course CUDA and OpenCL.

## How To
Basically all one needs to do is build a neural network such as the one in `main.cu`. Creating a suitable architecture is needed, so the output size of one linear layer has to be the input size of the next, but an exception is thrown if it is not. Then just compile everyting and run `./main`. One should see progress while training every tenth epoch.

## Architecture
I have all the layers in `GPU/layers/`. In those classes, I have implemented all the forward and backward passes of the different layers. 
In order to make things easier with allocating memory on host and device, I've decided to create a class called `Matrix.cu`, stored under `utils/matrix.cu`. This enables me to create the weights and bias matrix for layers very conveniently. This way, I don't have to worry about copying data between host and device. It also provides a very nice way to access the data in the matrix.
I also created a class `NeuralNetwork` which takes care of combining different layers. It also provides an accuracy metric and training function. 
The data I used is MNIST. In order for this to work, I had to one hot encode the y data beforehand. I also needed to implement `mnist.cu` which takes care of loading the data and storing it in a `Matrix`. 

## Problems 
Most of the calculations are happening on device but some were really hard to figure out, such as flattening a matrix. This is why only a few are done on CPU. 

The next problem is that I've tested updating the weights and bias in the linear layer, but somehow with the training dataset they are not updated when training the net. With the test dataset I saw that they are being updated but were converging relatively quickly. I've investigated this but I have not found a solution for this problem. It might be the more complex data versus the easy architecture, I don't really know and I have struggled very hard to find out. 

## Learnings
My goal with this project was to implement an architecture where one can build a neural network. I wanted to provide the opportunity to add different layers with different sizes in an easy way. This I have achieved. It was hard to implement the layers in a way, that everything is dynamically build and still can communicate very well with each other. Therefore, I have learned a lot about how important it is to have a good architecture.
I have also learned a great deal about neural networks and about building something from the ground up. When comparing to professional libraries for neural networks, I now can see the great amount of work put into those libraries and compared to how fast and efficient these are, one should always use provided libraries. Almost all of the time, they just provide so much better quality and are more efficient.
Even though I did not build a neural network that performs well on MNIST dataset, I am still proud of my work and maybe it actually performs well on binary classification tasks, which I have not tried to do. Therefore, one would probably need to add a different cost function.