#ifndef SIGMOID_H
#define SIGMOID_H

#include "layer.h"

class Sigmoid : public Layer{
    private:
        Matrix Output;
        Matrix Input;
        Matrix BackpropOutput;
    public:
        Sigmoid(std::string name);
        ~Sigmoid();

        Matrix forward(Matrix Input);
        Matrix backward(Matrix BackpropOutput, float learning_rate);
};

#endif