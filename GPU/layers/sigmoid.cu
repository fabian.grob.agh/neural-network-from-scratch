#include <iostream>
#include <stdlib.h>
#include "sigmoid.h"

// basic sigmoid function 
__device__ float sigmoid_gpu(float x){
    return 1.0f / ( 1 - exp(-x));
}

// actual forward pass on device
__global__ void sigmoid_forward_gpu(float* input, float* output, int inp_x_dim, int inp_y_dim){
    // calculating idiom
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i < inp_x_dim * inp_y_dim){
        // for each thread, which is always one number in the matrix, we call sigmoid on device
        output[i] = sigmoid_gpu(input[i]);
    }
}

//backward pass on device
__global__ void sigmoid_backward_gpu(float* input, float* dA, float* output, int inp_x_dim, int inp_y_dim){
    // calculating idiom
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i < inp_x_dim * inp_y_dim){
        // almost the same as forward
        output[i] = dA[i] * sigmoid_gpu(input[i]) * (1 - sigmoid_gpu(input[i]));
    }
}

Sigmoid::Sigmoid(std::string n) : Output(), Input(), BackpropOutput(){
    name = n;
}

Sigmoid::~Sigmoid()
{}

// forward pass wrapper
Matrix Sigmoid::forward(Matrix input){
    this->Input = input;
    Output.allocateMemory(input.x_dim, input.y_dim);
    Output.initConstant(0.0);

    dim3 block_size(BLOCK_SIZE);
	dim3 num_of_blocks((Input.y_dim * Input.x_dim + block_size.x - 1) / block_size.x);

    // calling the actual forward pass with input and output
    sigmoid_forward_gpu<<<num_of_blocks, block_size>>>(Input.data_device, Output.data_device, Input.x_dim, Input.y_dim);
    cudaDeviceSynchronize();
    Output.copyDeviceToHost();

    return Output;
}

Matrix Sigmoid::backward(Matrix dA, float lr){
    BackpropOutput.allocateMemory(Input.x_dim, Input.y_dim);
    BackpropOutput.initConstant(0.0);


    dim3 block_size(BLOCK_SIZE);
	dim3 num_of_blocks((Input.x_dim * Input.y_dim + block_size.x - 1) / block_size.x);
	sigmoid_backward_gpu<<<num_of_blocks, block_size>>>(Input.data_device, dA.data_device,
															 BackpropOutput.data_device,
															 Input.x_dim, Input.y_dim);
    cudaDeviceSynchronize();
    BackpropOutput.copyDeviceToHost();

    return BackpropOutput;
}
