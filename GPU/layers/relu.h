#ifndef RELU_H
#define RELU_H

#include "layer.h"

class ReLU : public Layer {
    private:
        Matrix Output;
        Matrix Input;
        Matrix BackpropOutput;
    public:
        ReLU(std::string name);
        ~ReLU();

        Matrix forward(Matrix Input);
        Matrix backward(Matrix BackpropOutput, float learning_rate);
};

#endif