#ifndef LAYER_H
#define LAYER_H

#define BLOCK_SIZE 28

#include "../../utils/matrix.h"

class Layer {
    public:
        virtual ~Layer() = 0;
        std::string name;
        virtual Matrix forward(Matrix Input) = 0;
        virtual Matrix backward(Matrix dZ, float learning_rate) = 0;

        std::string getName(){ return this->name; };
};

inline Layer::~Layer() {}

#endif