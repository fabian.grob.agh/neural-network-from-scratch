#include <stdlib.h>
#include <iostream>
#include "softmax.h"

__global__ void fold_matrix_gpu(float* matrix, float* result, int x_dim, int y_dim){
     
    int row = blockIdx.x * blockDim.x + threadIdx.x;

    if( row < x_dim){
        float val = 0.0f;
        for (int i = 0; i < y_dim; i++) {
            val += exp(matrix[row * y_dim + i]);
        }
        result[row] = val;
    }
}

Matrix fold_matrix(Matrix M){

    Matrix result(M.x_dim, 1);
    result.initConstant(0.0);
    result.copyHostToDevice();

    int threadsPerBlock = 256;
    int numBlocks = (M.x_dim + threadsPerBlock - 1) / threadsPerBlock;

    fold_matrix_gpu<<<numBlocks, threadsPerBlock>>>(M.data_device, result.data_device, M.x_dim, M.y_dim);
    cudaDeviceSynchronize();

    result.copyDeviceToHost();

    result.freeDevice();

    return result;
}

__global__ void softmax_gpu(float* matrix, float* folded_matrix, float* result, int x_dim, int y_dim){
    
    int row = blockIdx.x * blockDim.x + threadIdx.x;

    if( row < x_dim){
        for(int col = 0; col < y_dim; col++){
            result[row * y_dim + col] = exp(matrix[row * y_dim + col]) / folded_matrix[row];
        }
    }
}


Matrix Softmax::forward(Matrix M){
    this->Input = M;
    // std::cout << Input.x_dim << std::endl;
    Input.copyHostToDevice();
    Output.allocateMemory(Input.x_dim, Input.y_dim);
    Output.initConstant(0.0);

    Matrix folded_m = fold_matrix(M);

    int threadsPerBlock = 256;
    int numBlocks = (Input.x_dim + threadsPerBlock - 1) / threadsPerBlock;

    softmax_gpu<<<numBlocks, threadsPerBlock>>>(Input.data_device, folded_m.data_device, Output.data_device, Output.x_dim, Output.y_dim);
    cudaDeviceSynchronize();

    Output.copyDeviceToHost();

    return Output;
}


__global__ void backward_gpu(float* predictions, float* y, float* result, int x_dim, int y_dim){
    int row = blockIdx.x * blockDim.x + threadIdx.x;
    int col = blockIdx.y * blockDim.y + threadIdx.y;
    if (row < x_dim && col < y_dim){
        result[row * y_dim + col] = predictions[row * y_dim + col] - y[row * y_dim + col];
    }
}


Matrix Softmax::backward(Matrix Y, float learning_rate = 0.01){
    if(Y.x_dim != Output.x_dim || Y.y_dim != Output.y_dim){
        throw std::invalid_argument("Backward Pass Error! Dimensions not compatible in softmax layer");
    }
    Y.copyHostToDevice();
    BackpropOutput.allocateMemory(Y.x_dim, Y.y_dim);
    BackpropOutput.initConstant(0.0);

    dim3 threadsPerBlock(16,16);
    dim3 numBlocks((Y.x_dim + threadsPerBlock.x - 1)/ threadsPerBlock.x, (Y.y_dim + threadsPerBlock.y - 1)/ threadsPerBlock.y);

    backward_gpu<<<numBlocks, threadsPerBlock>>>(Output.data_device, Y.data_device, BackpropOutput.data_device, Y.x_dim, Y.y_dim);
    cudaDeviceSynchronize();

    BackpropOutput.copyDeviceToHost();

    return BackpropOutput;
}

Softmax::Softmax(std::string n) : Input(), Output(), BackpropOutput(){
    name = n;
}

Softmax::~Softmax(){}
