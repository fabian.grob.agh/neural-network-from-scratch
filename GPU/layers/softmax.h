#ifndef SOFTMAX_H
#define SOFTMAX_H

#include "layer.h"

class Softmax : public Layer{
    private:
        Matrix Output;
        Matrix Input;
        Matrix BackpropOutput;
    public:
        Softmax(std::string name);
        ~Softmax();

        Matrix forward(Matrix Input);
        Matrix backward(Matrix BackpropOutput, float learning_rate);
};

#endif