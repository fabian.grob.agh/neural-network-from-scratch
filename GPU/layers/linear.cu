#include <stdlib.h>
#include <iostream>
#include <stdexcept>

#include "linear.h"

// checked
__global__ void linear_forward_gpu(float* input, float* weights, float* output, float* bias, int inp_x_dim, int inp_y_dim,
                                    int w_x_dim, int w_y_dim){
    
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    int out_x_dim = inp_x_dim;
    int out_y_dim = w_y_dim;

    if(col < out_x_dim && row < out_y_dim){
        float tmp = 0.0f;
        for(int i = 0; i < w_x_dim; i++){
            tmp += input[row * inp_y_dim + i] * weights[i * w_y_dim + col];
        }
        output[col * out_y_dim + row] = tmp + bias[row];
    }
}

// backward pass accordingly to dA = weight_T * dZ, dZ is error of prev layer
// CHECKED!
__global__ void linear_backward_gpu(float* weights, float* dZ, float* dA, int w_x_dim, int w_y_dim, int dZ_x_dim, int dZ_y_dim){
    
    int row = blockIdx.y * blockDim.y + threadIdx.y;
    int col = blockIdx.x * blockDim.x + threadIdx.x;

    // setting the shape of input Error matrix
    int dA_x_dim = dZ_x_dim;
	int dA_y_dim = w_x_dim;

    float res = 0;

    if(col < dA_y_dim && row < dA_x_dim){
		for (int i = 0; i < w_y_dim; i++) {
			res += dZ[row * dZ_y_dim + i] * weights[col * w_y_dim + i];
		}
		dA[row * dA_y_dim + col] = res;
	}
}    

// updating weights accordingly to dW = 1/m * dZ * input_T
// CHECKED!
__global__ void update_weights_gpu( float* dZ, float* input, float* w, int dZ_x_dim, int dZ_y_dim,
									int inp_x_dim, int inp_y_dim,
									float learning_rate){
    
    int col = blockIdx.x * blockDim.x + threadIdx.x;
	int row = blockIdx.y * blockDim.y + threadIdx.y;

    int w_x_dim = inp_y_dim;
    int w_y_dim = dZ_y_dim;

    float dW = 0;

    if(row < w_x_dim && col < w_y_dim){
        for(int i = 0; i < dZ_x_dim; i++){
            dW += input[i * inp_y_dim + row] * dZ[i * dZ_y_dim + col];
        }
        // updating weights by substracting dW with learning rate
        w[row * w_y_dim + col] = w[row * w_y_dim + col] - learning_rate * (dW / inp_x_dim);
    }
}

float Linear::sumMatrix(Matrix m){
    float res = 0;
    for(int row = 0; row < m.x_dim; row++){
        for(int col = 0; col < m.y_dim; col++){
            res += m(row, col);
        }
    }
    return res;
}

// CHECKED!
__global__ void update_bias_gpu(float* bias, int b_x_dim, int m, float sumOfError, float learning_rate){

    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i < b_x_dim){
        atomicAdd(&bias[i], - learning_rate * (sumOfError / m));
    }
}

Linear::Linear(std::string name, size_t x, size_t y) : weights(x, y),  bias(x, 1){
    
    this->name = name;

    bias.initRandom(0.1);
    weights.initRandom(0.1);
}

Linear::~Linear(){
}

// CHECKED! 
Matrix Linear::forward(Matrix input){
    // making sure we can do matmul
    if(input.y_dim != weights.x_dim){
        throw std::invalid_argument( "Input dimension not compatible with layer dimension!" );
    }

    this->Input = input;

    Output.allocateMemory(Input.x_dim, weights.y_dim);
    Output.copyHostToDevice();
    Input.copyHostToDevice();
    weights.copyHostToDevice();
    bias.copyHostToDevice();

    dim3 threadsPerBlock(32, 32);
	dim3 numBlocks(	((Input.x_dim + threadsPerBlock.x - 1) / threadsPerBlock.x), ((weights.y_dim + threadsPerBlock.y - 1) / threadsPerBlock.y));
	linear_forward_gpu<<<numBlocks, threadsPerBlock>>>(Input.data_device,
													   weights.data_device,
													   Output.data_device,
													   bias.data_device,
													   Input.x_dim, Input.y_dim,
													   weights.x_dim, weights.y_dim);
    cudaDeviceSynchronize();

    Output.copyDeviceToHost();
    Input.copyDeviceToHost();
    weights.copyDeviceToHost();
    bias.copyDeviceToHost();

    return Output;
}


Matrix Linear::backward(Matrix dOutput, float learning_rate){
    dInput.allocateMemory(Input.x_dim, Input.y_dim);
    dInput.copyHostToDevice();
    dOutput.copyHostToDevice();
    weights.copyHostToDevice();
    bias.copyHostToDevice();
    Input.copyHostToDevice();
    //backward pass
    dim3 block_size(BLOCK_SIZE, BLOCK_SIZE);
	dim3 num_of_blocks(	(Output.x_dim + block_size.x - 1) / block_size.x,
						(Output.y_dim + block_size.y - 1) / block_size.y);
	linear_backward_gpu<<<num_of_blocks, block_size>>>( weights.data_device,
														dOutput.data_device,
														dInput.data_device,
														weights.x_dim, weights.y_dim,
													    dOutput.x_dim, dOutput.y_dim);
    weights.copyDeviceToHost();
    dOutput.copyDeviceToHost();
    dInput.copyDeviceToHost();

    // updating bias
    float errorSum = this->sumMatrix(dOutput);
    dim3 block_size_bias(BLOCK_SIZE);
	dim3 num_of_blocks_bias( (bias.x_dim + block_size_bias.x - 1) / block_size_bias.x);
	update_bias_gpu<<<num_of_blocks_bias, block_size_bias>>>(bias.data_device,
														    bias.x_dim,
														 Input.x_dim, errorSum, learning_rate);

    bias.copyDeviceToHost();

    // updating weights
    dim3 block_size_w(BLOCK_SIZE, BLOCK_SIZE);
	dim3 num_of_blocks_w((weights.x_dim + block_size_w.x - 1) / block_size_w.x,
						(weights.y_dim + block_size_w.y - 1) / block_size_w.y);
	update_weights_gpu<<<num_of_blocks_w, block_size_w>>>(dOutput.data_device,
															Input.data_device,
															weights.data_device,
															dOutput.x_dim, dOutput.y_dim,
															Input.x_dim, Input.y_dim,
															learning_rate);
    cudaDeviceSynchronize();

    Input.copyDeviceToHost();
    dInput.copyDeviceToHost();
    dOutput.copyDeviceToHost();
    weights.copyDeviceToHost();
    bias.copyDeviceToHost();

    return dInput;
}

int Linear::getX() const {
	return weights.x_dim;
}

int Linear::getY() const {
	return weights.y_dim;
}

Matrix Linear::getWeights() const {
	return weights;
}

Matrix Linear::getBias() const {
	return bias;
}

void Linear::setBias(Matrix M){
    this->bias = M;
}

void Linear::setWeights(Matrix M){
    this->weights = M;
}
