#ifndef LINEAR_H
#define LINEAR_H

#include "layer.h"

class Linear : public Layer{
    private:
        // we need weights, bias
        Matrix weights, bias;
        int bs, n_in, n_out;

        Matrix Input, Output;
        // for backpropagation
        Matrix dInput;

        void update_params(Matrix dOutput, float learning_rate);
    public:
        Linear(std::string name, size_t x, size_t y);
        ~Linear();

        Matrix forward(Matrix input);
        Matrix backward(Matrix dOutput, float learning_rate = 0.1);

        // getter funcs for other layers to know sizes
        int getX() const;
        int getY() const;

        Matrix getWeights() const;
        Matrix getBias() const;
        void setBias(Matrix M);
        void setWeights(Matrix M);
        void updateBiasTest(Matrix w, Matrix err, int batch_size, float lr);
        float sumMatrix(Matrix m);
};

#endif