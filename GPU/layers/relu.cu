#include <stdlib.h>
#include <iostream>
#include "relu.h"

// forward pass, implementing the basic ReLU function
__global__ void relu_forward_gpu(float* input, float* output, int inp_x_dim, int inp_y_dim){
    // calculating idiom
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i < inp_x_dim * inp_y_dim){
        output[i] = fmaxf(input[i], 0);
    }
}

__global__ void relu_backward_gpu(float* input, float* dA, float* output, int inp_x_dim, int inp_y_dim){
    // calculating idiom
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if(i < inp_x_dim * inp_y_dim){
        if(input[i] > 0){
            output[i] = dA[i];
        } else{
            output[i] = 0;
        }
    }
}

ReLU::ReLU(std::string n) : Output(), Input(), BackpropOutput(){
    this->name = n;
}

ReLU::~ReLU(){}

Matrix  ReLU::forward(Matrix inp){
    this->Input = inp;

    Output.allocateMemory(Input.x_dim, Input.y_dim);
    Output.initConstant(0.0);

    Input.copyHostToDevice();
    Output.copyHostToDevice();

    dim3 block_size(BLOCK_SIZE);
	dim3 num_of_blocks((Input.y_dim * Input.x_dim + block_size.x - 1) / block_size.x);

    relu_forward_gpu<<<num_of_blocks, block_size>>>(Input.data_device, Output.data_device, Input.x_dim, Input.y_dim);
    Output.copyDeviceToHost();

    return Output;
}
 
Matrix ReLU::backward(Matrix dA, float lr){
    BackpropOutput.allocateMemory(Input.x_dim, Input.y_dim);
    BackpropOutput.copyHostToDevice();
    dA.copyHostToDevice();

    dim3 block_size(BLOCK_SIZE);
	dim3 num_of_blocks((Input.x_dim * Input.y_dim + block_size.x - 1) / block_size.x);
	relu_backward_gpu<<<num_of_blocks, block_size>>>(Input.data_device, dA.data_device,
															 BackpropOutput.data_device,
															 Input.x_dim, Input.y_dim);

    BackpropOutput.copyDeviceToHost();
    dA.copyDeviceToHost();

    return BackpropOutput;
}