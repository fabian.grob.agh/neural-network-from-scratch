#include <iostream>
#include <stdlib.h>
#include "neuralNetwork.h"

NeuralNetwork::NeuralNetwork(float learning_rate) :
	learning_rate(learning_rate)
{ }

NeuralNetwork::~NeuralNetwork() {
	for (auto layer : layers) {
		delete layer;
	}
}

void NeuralNetwork::addLayer(Layer* layer) {
	this->layers.push_back(layer);
}

Matrix NeuralNetwork::forward(Matrix X) {
	Matrix Z = X;

	for (auto layer : layers) {
		Z = layer->forward(Z);
	}

	Y = Z;
	return Y;
}

void NeuralNetwork::backward(Matrix predictions, Matrix target) {
	Matrix error = predictions;
    // for the softmax layer, the backward function is also the cost function for this whole neural net.
	for (auto it = this->layers.rbegin(); it != this->layers.rend(); it++) {
		error = (*it)->backward(target, learning_rate);
	}

	cudaDeviceSynchronize();
}

void NeuralNetwork::train(Matrix X_train, Matrix Y_train, int epochs){
    for(int epoch = 0; epoch < epochs + 1; epoch++){
        Y = this->forward(X_train);
        this->backward(Y, Y_train);

        Y.copyDeviceToHost();
        
        if( epoch % 10 == 0){
            std::cout << "Epoch:\t" << epoch << ",\tAccuracy:\t" << accuracy(Y, Y_train) << std::endl;
        }
    }
}

Matrix NeuralNetwork::predict(Matrix X){
    Matrix prediction = this->forward(X);
    return prediction;
}

std::vector<Layer*> NeuralNetwork::getLayers() const {
	return layers;
}

float NeuralNetwork::accuracy(Matrix predictions, Matrix Y_true){
    int total = Y_true.x_dim;

    Matrix predicts_rounded = predictions.roundOutput();

    int false_classified = 0;
    
    for(int row = 0; row < Y_true.x_dim; row++){
        bool row_false = false;
        for(int col = 0; col < Y_true.y_dim; col++){
            if(!row_false && Y_true(row, col) != predicts_rounded(row, col)){
                false_classified += 1;
                row_false = true;
            }
        }
    }

    int correct = total - false_classified;
    float accuracy = float(correct) / float(total);

    return accuracy;
}

float NeuralNetwork::getLearningRate(){
    return this->learning_rate;
}