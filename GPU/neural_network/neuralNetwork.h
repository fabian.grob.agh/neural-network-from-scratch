#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#include "../layers/layer.h"
#include <vector>

class NeuralNetwork {
private:
	std::vector<Layer*> layers;

	Matrix Y;
	Matrix dY;
	float learning_rate;

public:
	NeuralNetwork(float learning_rate = 0.001);
	~NeuralNetwork();

	Matrix forward(Matrix X);
	void backward(Matrix predictions, Matrix target);

    void train(Matrix X_train, Matrix Y_train, int epochs = 1000);

	void addLayer(Layer *layer);
	std::vector<Layer*> getLayers() const;

	Matrix predict(Matrix X);

    float accuracy(Matrix predictions, Matrix Y_true);
	float getLearningRate();
};


#endif