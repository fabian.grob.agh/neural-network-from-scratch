#include <stdlib.h>
#include <iostream>
#include "data/mnist.h"
#include "GPU/neural_network/neuralNetwork.h"
#include "GPU/layers/linear.h"
#include "GPU/layers/relu.h"
#include "GPU/layers/sigmoid.h"
#include "GPU/layers/softmax.h"

int main(){
    MNIST* mnist = new MNIST();
    
    Matrix X_train = mnist->getX_train();
    Matrix y_train = mnist->getY_train();
    Matrix X_test = mnist->getX_test();
    Matrix y_test = mnist->getY_test();

    // preprocessing the data
    X_train.divideData(255.0);
    X_test.divideData(255.0);

    // building the neural network
    NeuralNetwork nn;
    nn.addLayer(new Linear("linear_1", 784, 256));
    nn.addLayer(new ReLU("relu_1"));
    nn.addLayer(new Linear("linear_2", 256, 128));
    nn.addLayer(new Sigmoid("sigmoid_1"));
    nn.addLayer(new Linear("linear_3", 128, 10));
    nn.addLayer(new Softmax("softmax"));

    // training
    nn.train(X_train, y_train, 1000);

    // test predictions
    Matrix predictions = nn.predict(X_test);

    // printing the first 10 results
    for(int row = 0; row < 10; row++){
        std::cout << "Row " << row << ": ";
        for(int col = 0; col < predictions.y_dim; col++){
            std::cout << predictions(row, col) << "  ";
        }
        std::cout << std::endl;
    }


    // accuracy
    std::cout << "\nACCURACY TEST SET: " << nn.accuracy(X_test, y_test) << std::endl;



    return 0;
}