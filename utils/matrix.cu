#include "matrix.h"
#include <stdexcept>
#include <iostream>
#include <random>

Matrix::Matrix(size_t x, size_t y){
    x_dim = x;
    y_dim = y;
    device_allocated = false;
    host_allocated = false;
    data_host = nullptr;
    data_device = nullptr;
    allocateMemory();
}

Matrix::Matrix(){
    device_allocated = false;
    host_allocated = false;
    data_host = nullptr;
    data_device = nullptr;
}

Matrix::~Matrix(){
    // cudaFree(data_device);
}

void Matrix::allocateDeviceMemory(){
    if(!device_allocated){
        int bytes = x_dim * y_dim * sizeof(float);
        cudaMalloc(&data_device, bytes);
        device_allocated = true;
    }
}

void Matrix::allocateHostMemory(){
    if(!host_allocated){
        data_host = new float[x_dim * y_dim];
        host_allocated = true;
    }
}

void Matrix::allocateMemory(int x, int y){
    if(!host_allocated && !device_allocated){
        this->x_dim = x;
        this->y_dim = y;
        allocateMemory();
    }
}

void Matrix::allocateMemory(){
    allocateDeviceMemory();
    allocateHostMemory();
}

void Matrix::freeDevice(){
    if(device_allocated){
        cudaFree(data_device);
        device_allocated = false;
    }
}

void Matrix::copyHostToDevice() {
    int bytes = x_dim * y_dim * sizeof(float);
    cudaMemcpy(data_device, data_host, bytes, cudaMemcpyHostToDevice);
}


void Matrix::copyDeviceToHost() {
    int bytes = x_dim * y_dim * sizeof(float);
    cudaMemcpy(data_host, data_device, bytes, cudaMemcpyDeviceToHost);
}

void Matrix::initConstant(float c){
    for(int i = 0; i < x_dim; i++){
        for(int j = 0; j < y_dim; j++){
            data_host[i * y_dim + j] = c; 
        }
    }
    copyHostToDevice();
}


void Matrix::initRandom(float m){
    std::default_random_engine gen;
	std::normal_distribution<float> normal_distribution(0.0, 1.0);

    for(int i = 0; i < x_dim; i++){
        for(int j = 0; j < y_dim; j++){
            data_host[i * y_dim + j] = normal_distribution(gen) * m;
        }
    }
	copyHostToDevice();
}


__global__ void roundOutput_gpu(float* input, float* output, int x_dim, int y_dim){
    int row = blockIdx.x * blockDim.x + threadIdx.x;

    if( row < x_dim){
        int best_col = 0;
        for (int i = 1; i < y_dim; i++) {
            if(input[row * y_dim + best_col] < input[row * y_dim + i]){
                output[row * y_dim + best_col] = 0;
                best_col = i;
            }
        }
        output[row * y_dim + best_col] = 1;
    }
}

Matrix Matrix::roundOutput(){
    Matrix res(x_dim, y_dim);
    res.initConstant(0.0);
    copyHostToDevice();

    int threadsPerBlock = 256;
    int numBlocks = (x_dim + threadsPerBlock - 1) / threadsPerBlock;

    roundOutput_gpu<<<numBlocks, threadsPerBlock>>>(data_device, res.data_device, x_dim, y_dim);
    cudaDeviceSynchronize();

    res.copyDeviceToHost();

    res.freeDevice();

    return res;
}

__global__ void divideData_gpu(float* input, float c, int x_dim, int y_dim){
    int row = blockIdx.x * blockDim.x + threadIdx.x;

    if( row < x_dim){
        for (int i = 0; i < y_dim; i++) {
            input[row * y_dim + i] = input[row * y_dim + i] / c;
        }
    }
}

void Matrix::divideData(float c){

    int threadsPerBlock = 256;
    int numBlocks = (x_dim + threadsPerBlock - 1) / threadsPerBlock;

    divideData_gpu<<<numBlocks, threadsPerBlock>>>(data_device, c, x_dim, y_dim);
    cudaDeviceSynchronize();

    copyDeviceToHost();
}


float& Matrix::operator[](const int flat_index) {
    return data_host[flat_index];
}

const float& Matrix::operator[](const int flat_index) const {
    return data_host[flat_index];
}

float& Matrix::operator()(const int i, const int j) {
	if (i >= x_dim || j >= y_dim)
        throw std::out_of_range("Matrix subscript out of bounds");
    return data_host[i * y_dim + j];
}

const float& Matrix::operator()(const int i, const int j) const {
	if (i >= x_dim || j >= y_dim)
        throw std::out_of_range("Matrix subscript out of bounds");
    return data_host[i * y_dim + j];
}