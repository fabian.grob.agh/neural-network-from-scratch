#ifndef MATRIX_H
#define MATRIX_H

#include <memory>

class Matrix{
    private:
        bool device_allocated;
        bool host_allocated;

        void allocateDeviceMemory();
        void allocateHostMemory();

    public:
        int x_dim, y_dim;

        float *data_device;
        float *data_host;

        void allocateMemory();
        void allocateMemory(int x, int y);
        void freeDevice();
        
        void initConstant(float c);
        void initRandom(float m);

        void copyHostToDevice();
        void copyDeviceToHost();

        Matrix(size_t x, size_t y);
        Matrix();
        ~Matrix();

        Matrix roundOutput();
        void divideData(float c);

        float& operator[](const int flat_index);
        const float& operator[](const int flat_index) const;
        float& operator()(const int i, const int j);
        const float& operator()(const int i, const int j) const;
};

#endif