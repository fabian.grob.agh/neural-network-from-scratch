#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include "../utils/matrix.h"
#include "mnist.h"

std::vector<std::vector<std::string> > MNIST::parseRawCSV(std::string filePath){
    std::ifstream data(filePath);
    std::string line; 
    std::vector<std::vector<std::string> > parsedCsv;
    while(std::getline(data,line)){
        std::istringstream lineStream(line);
        std::string cell; 
        std::vector<std::string> parsedRow;
        while(std::getline(lineStream, cell, ',')){
            parsedRow.push_back(cell);
        }

        parsedCsv.push_back(parsedRow);
    }

    return parsedCsv;
}

std::vector<std::vector<float> > MNIST::convertToFloat(std::vector<std::vector<std::string> > raw){
    std::vector<std::vector<float> > converted;

    std::vector< std::vector<std::string> >::const_iterator row; 
    std::vector<std::string>::const_iterator col; 

    for (row = raw.begin(); row != raw.end(); ++row)
    { 
        std::vector<float> convertedRow;
        for (col = row->begin(); col != row->end(); ++col)
        { 
            std::stringstream parser(*col);
            float x = 0;

            parser >> x;

            convertedRow.push_back(x);
        } 
        converted.push_back(convertedRow);
    } 

    return converted;
}

float* MNIST::vectorToArray(std::vector<std::vector<float> > &vals, int X, int Y){
   float* temp = new float[X * Y];
   for(int row = 0; row < X; row++)
   { 
      for(int col = 0; col < Y; col++)
      {
          temp[row * Y + col] = vals[row][col];
      } 
   }

   return temp;
 }


Matrix MNIST::convertToMatrix(std::vector<std::vector<std::string> > raw){
    std::vector<std::vector<float> > floatVector = convertToFloat(raw);
    float* intArray = vectorToArray(floatVector, floatVector.size(), floatVector[0].size());

    Matrix res(floatVector.size(), floatVector[0].size());
    res.data_host = intArray;
    res.copyHostToDevice();

    return res;
}

Matrix MNIST::parseCSV(std::string filePath){
    return convertToMatrix(parseRawCSV(filePath));
}


Matrix MNIST::getX_train(){
    return parseCSV("./data/X_train.csv");
}

Matrix MNIST::getY_train(){
    return parseCSV("./data/y_train.csv");
}

Matrix MNIST::getX_test(){
    return parseCSV("./data/X_test.csv");
}

Matrix MNIST::getY_test(){
    return parseCSV("./data/y_test.csv");
}
