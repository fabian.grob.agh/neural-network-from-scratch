#ifndef MNIST_H
#define MNIST_H

#include "../utils/matrix.h"
#include <vector>

class MNIST {
    private:
        Matrix convertToMatrix(std::vector<std::vector<std::string> > raw);
        float* vectorToArray(std::vector<std::vector<float> > &vals, int X, int Y);
        std::vector<std::vector<float> > convertToFloat(std::vector<std::vector<std::string> > raw);
        std::vector<std::vector<std::string> > parseRawCSV(std::string filePath);
    public:
        Matrix parseCSV(std::string file);
        Matrix getX_train();
        Matrix getY_train();
        Matrix getX_test();
        Matrix getY_test();
};

#endif